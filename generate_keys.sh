#! /bin/sh
# $1: destination dir ; $2: key_prefix
mkdir "$1"
cd "$1"
openssl genrsa 2048 > "$2"_ciph_rsa2048_priv.pem
openssl genrsa 2048 > "$2"_sign_rsa2048_priv.pem
openssl rsa -in "$2"_ciph_rsa2048_priv.pem -pubout > "$2"_ciph_rsa2048_pub.pem
openssl rsa -in "$2"_sign_rsa2048_priv.pem -pubout > "$2"_sign_rsa2048_pub.pem
cd $OLDPWD
