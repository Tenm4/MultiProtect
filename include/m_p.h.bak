
# include "mbedtls/pk.h"

# include <stdlib.h>

int print_hex (unsigned char *buffer, int buffer_len, char *id);

void zeroize (void *v, size_t n);

int load_pk_key (mbedtls_pk_context *pk_ctx, int is_public, const char *path, const char *password);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        ciphered text buffer
* [in]  input_len    ciphered text buffer length in bytes
* [in]  symkey       symetric key (32 bytes)
* [in]  symkey_len   symetric key length in bytes
* [in]  iv           initialization vector (16 bytes)
* [in]  iv_len       initialization vector length in bytes
* return 0 if OK, 1 else
* 
* SYM :: AES-256-CBC
* PADDING :: 0x80
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int encrypt_buffer (unsigned char **output, int *output_len, unsigned char *input, int input_len, unsigned char *symkey, int symkey_len, unsigned char *iv, int iv_len);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        ciphered text buffer
* [in]  input_len    ciphered text buffer length in bytes
* [in]  symkey       symetric key (32 bytes)
* [in]  symkey_len   symetric key length in bytes
* [in]  iv           initialization vector (16 bytes)
* [in]  iv_len       initialization vector length in byte
* return 0 if OK, 1 else
* 
* SYM :: AES-256-CBC
* PADDING :: 0x80
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int decrypt_buffer(unsigned char **output, int *output_len, unsigned char *input, int input_len, unsigned char *symkey, int symkey_len, unsigned char *iv, int iv_len);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        symetric key (32 bytes)
* [in]  input_len    symetric key length in bytes
* [in]  pubkey       public key context
* return 0 if OK, 1 else
* 
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int register_symkey (unsigned char **output, int *output_len, unsigned char *input, int input_len, mbedtls_pk_context *pubkey_ctx);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        symetric key (32 bytes)
* [in]  input_len    symetric key length in bytes
* [in]  privkey      private key context
* return 0 if OK, 1 else
* 
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int authenticate_symkey (unsigned char **output, int *output_len, unsigned char *input, int input_len, mbedtls_pk_context *privkey_ctx);

/**
* [out] output       signature buffer
* [in]  input        text buffer
* [in]  input_len    text buffer length in bytes
* [in]  pri_key_file private parameters file
* return 0 if OK, 1 else
* 
* HASH :: SHA-256
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int sign(unsigned char *output, unsigned char *input, int input_len, char *pri_key_file);

/**
* [in] sign         signature buffer
* [in] input        text buffer
* [in] input_len    text buffer length in bytes
* [in] pub_key_file public parameters file
* return 0 if OK, 1 else
* 
* HASH :: SHA-256
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int verif(unsigned char *sign, unsigned char *input, int input_len, char *pub_key_file);
