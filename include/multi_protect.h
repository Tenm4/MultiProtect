
# include "mbedtls/pk.h"

# include <stdlib.h>

# define RSA2048_BLOCK_SIZE 256
# define AES_BLOCK_SIZE 16
# define HASH_SIZE 32

int print_hex (unsigned char *buffer, int buffer_len, char *id);

void zeroize (void *v, size_t n);

int load_pk_key (mbedtls_pk_context *pk_ctx, int is_public, const char *path, const char *password);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        ciphered text buffer
* [in]  input_len    ciphered text buffer length in bytes
* [in]  symkey       symetric key (32 bytes)
* [in]  symkey_len   symetric key length in bytes
* [in]  iv           initialization vector (16 bytes)
* [in]  iv_len       initialization vector length in bytes
* return 0 if OK, 1 else
* 
* SYM :: AES-256-CBC
* PADDING :: 0x80
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int encrypt_buffer (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, unsigned char *symkey, size_t symkey_len, unsigned char *iv, size_t iv_len);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        ciphered text buffer
* [in]  input_len    ciphered text buffer length in bytes
* [in]  symkey       symetric key (32 bytes)
* [in]  symkey_len   symetric key length in bytes
* [in]  iv           initialization vector (16 bytes)
* [in]  iv_len       initialization vector length in byte
* return 0 if OK, 1 else
* 
* SYM :: AES-256-CBC
* PADDING :: 0x80
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int decrypt_buffer(unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, unsigned char *symkey, size_t symkey_len, unsigned char *iv, size_t iv_len);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        symetric key (32 bytes)
* [in]  input_len    symetric key length in bytes
* [in]  pubkey       public key context
* return 0 if OK, 1 else
* 
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int register_symkey (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, mbedtls_pk_context *pubkey_ctx);

/**
* [out] output       cipher text buffer
* [out] output_len   cipher text buffer length in bytes
* [in]  input        symetric key (32 bytes)
* [in]  input_len    symetric key length in bytes
* [in]  privkey      private key context
* return 0 if OK, 1 else
* 
* ASYM :: RSA-2048 (PKCS#1 v2.1)
*/
int authenticate_symkey (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, mbedtls_pk_context *privkey_ctx);

/**
* [out] sign         signature buffer
* [out] sign_len     signature buffer length in bytes
* [in]  input        text buffer
* [in]  input_len    text buffer length in bytes
* [in]  privkey_ctx  private key context
* return 0 if OK, 1 else
* 
* HASH :: SHA-256
* ASYM :: RSA-2048 (PKCS#1 v2.1) PSS
*/
int sign_pss (unsigned char **sign, size_t *sign_len, unsigned char *input, size_t input_len, mbedtls_pk_context *privkey_ctx);

/**
* [in] sign         signature buffer
* [in] sign_len     signature buffer length in bytes
* [in] input        text buffer
* [in] input_len    text buffer length in bytes
* [in] pubkey_ctx   public key context
* return 0 if OK, 1 else
* 
* HASH :: SHA-256
* ASYM :: RSA-2048 (PKCS#1 v2.1) PSS
*/
int verify_pss (unsigned char *sign, size_t sign_len, unsigned char *input, size_t input_len, mbedtls_pk_context *pubkey_ctx);
