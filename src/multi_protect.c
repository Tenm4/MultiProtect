# include "multi_protect.h"

# include "mbedtls/md.h"
# include "mbedtls/aes.h"
# include "mbedtls/entropy.h"
# include "mbedtls/ctr_drbg.h"

# include <stdio.h>
# include <string.h>


int print_hex (unsigned char *buffer, int buffer_len, char *id)
{
    int i = 0;

    if (id != NULL)
        printf(">>> %s\n", id);
    
    for (i = 0; i < buffer_len; i++)
        printf("%02X", buffer[i]);
    printf("\n");
    
    return 0;
}

void zeroize (void *v, size_t n)
{
    volatile unsigned char *p = v;
    while (n--) *p++ = 0;
}

int load_pk_key (mbedtls_pk_context *pk_ctx, int is_public, const char *path, const char *password)
{
    int ret, end_ret = 1;
    
    if (is_public)
    {
        ret = mbedtls_pk_parse_public_keyfile(pk_ctx, path);
        //printf("load_pk_key: mbedtls_pk_parse_public_keyfile returned %d\n", ret);
        if (ret != 0) {
            printf("Failed to extract public key from file %s!\n", path);
            goto load_pk_key_end;
        } else {
            printf("Successfully extracted public key from file %s!\n", path);
        }
    } else {
        ret = mbedtls_pk_parse_keyfile(pk_ctx, path, password);
        //printf("load_pk_key: mbedtls_pk_parse_keyfile returned %d\n", ret);
        if (ret != 0) {
            printf("Failed to extract private key from file %s!\n", path);
            goto load_pk_key_end;
        } else {
            printf("Successfully extracted private key from file %s!\n", path);
        }
    }
    
    ret = mbedtls_pk_can_do(pk_ctx, MBEDTLS_PK_RSA);
    if (ret != 1) {
        printf("RSA is not supported!\n");
        goto load_pk_key_end;
    }
    
    end_ret = 0;
    
load_pk_key_end:
    return end_ret;
}


int register_symkey (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, mbedtls_pk_context *pubkey_ctx)
{
    mbedtls_entropy_context entropy_ctx;
    mbedtls_ctr_drbg_context ctr_drbg_ctx;
    int ret, end_ret = 1;
    
    if (output == NULL || output_len == NULL || input == NULL || input_len <= 0 || pubkey_ctx == NULL)
        goto rsaenc_end;
    //printf("After tests\n");
    
    memset(&entropy_ctx, 0, sizeof(entropy_ctx));
    memset(&ctr_drbg_ctx, 0, sizeof(ctr_drbg_ctx));
    
    // RNG initialization
    mbedtls_entropy_init(&entropy_ctx);
    ret = mbedtls_ctr_drbg_seed(&ctr_drbg_ctx, mbedtls_entropy_func, &entropy_ctx, NULL, 0);
    if (ret != 0) { goto rsaenc_cleanup; }
    
    // Output alloc
    *output_len = RSA2048_BLOCK_SIZE;
    if (*output == NULL)
    {
        *output = calloc(*output_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (*output == NULL));
        if (*output == NULL) goto rsaenc_cleanup;
    }
    //else printf("After output alloc (already allocated)\n");
    
    // Encrypt sym key with public asym key
    mbedtls_rsa_set_padding(mbedtls_pk_rsa(*pubkey_ctx), MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);
    
    ret = mbedtls_pk_encrypt(pubkey_ctx, input, input_len, *output, output_len, *output_len, mbedtls_ctr_drbg_random, &ctr_drbg_ctx);
        
    //printf("After RSA encrypt of input: ret=%d\n", ret);
    if (ret != 0) { goto rsaenc_cleanup; }
    //printf("Output length: %lu\n", *output_len);
    //print_hex(*output, *output_len, "Registered symkey:");
    
    end_ret = 0;
    
rsaenc_cleanup:
    mbedtls_ctr_drbg_free(&ctr_drbg_ctx);
    mbedtls_entropy_free(&entropy_ctx);
    
rsaenc_end:
    return end_ret;
}


int authenticate_symkey (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, mbedtls_pk_context *privkey_ctx)
{
    mbedtls_entropy_context entropy_ctx;
    mbedtls_ctr_drbg_context ctr_drbg_ctx;
    int ret, end_ret = 1;
    
    if (output == NULL || output_len == NULL || input == NULL || input_len <= 0 || privkey_ctx == NULL)
        goto rsadec_end;
    //printf("After tests\n");
    
    memset(&entropy_ctx, 0, sizeof(entropy_ctx));
    memset(&ctr_drbg_ctx, 0, sizeof(ctr_drbg_ctx));
    
    // RNG initialization
    mbedtls_entropy_init(&entropy_ctx);
    ret = mbedtls_ctr_drbg_seed(&ctr_drbg_ctx, mbedtls_entropy_func, &entropy_ctx, NULL, 0);
    if (ret != 0) { goto rsadec_cleanup; }
    
    // Output alloc
    *output_len = RSA2048_BLOCK_SIZE;
    if (*output == NULL)
    {
        *output = calloc(*output_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (*output == NULL));
        if (*output == NULL) goto rsadec_cleanup;
    }
    //else printf("After output alloc (already allocated)\n");
    
    // Encrypt sym key with public asym key
    mbedtls_rsa_set_padding(mbedtls_pk_rsa(*privkey_ctx), MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);
    
    ret = mbedtls_pk_decrypt(privkey_ctx, input, input_len, *output, output_len, *output_len, mbedtls_ctr_drbg_random, &ctr_drbg_ctx);
    //printf("After RSA decrypt of input: ret=%d\n", ret);
    if (ret != 0) { goto rsadec_cleanup; }
    //printf("Output length: %lu\n", *output_len);
    //print_hex(*output, *output_len, "Authenticated symkey:");
    
    end_ret = 0;
    
rsadec_cleanup:
    mbedtls_ctr_drbg_free(&ctr_drbg_ctx);
    mbedtls_entropy_free(&entropy_ctx);
    
rsadec_end:
    return end_ret;
}


int encrypt_buffer (unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, unsigned char *symkey, size_t symkey_len, unsigned char *iv, size_t iv_len)
{
    mbedtls_aes_context aes_ctx;
    int ret, end_ret = 1;
    int plain_text_len = input_len + (AES_BLOCK_SIZE - input_len % AES_BLOCK_SIZE); // Eager padding
    unsigned char i, plain_text[plain_text_len];
    
    if (output == NULL || output_len == NULL || input == NULL || input_len <= 0 || symkey == NULL || symkey_len <= 0 || iv == NULL || iv_len <= 0)
        goto encrypt_end;
    //printf("After tests\n");
    
    // Pad plain
    memcpy(plain_text, input, input_len);
    if (input_len + 1 <= plain_text_len)
        plain_text[input_len] = 0x80;
    for (i = input_len + 1; i < plain_text_len; i++)
        plain_text[i] = 0;
    //printf("Padded plaintext length: %d\n", plain_text_len);
    //print_hex(plain_text, plain_text_len, "Padded plaintext:");
    
    // Output alloc
    *output_len = plain_text_len;
    if (*output == NULL)
    {
        *output = calloc(*output_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (*output == NULL));
        if (*output == NULL) goto encrypt_end;
    }
    //else printf("After output alloc (already allocated)\n");
    
    // Encrypt plain text with sym key
    mbedtls_aes_init(&aes_ctx);
    
    ret = mbedtls_aes_setkey_enc(&aes_ctx, symkey, symkey_len * 8);
    if (ret != 0) { goto encrypt_aes_cleanup; }
    
    ret = mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_ENCRYPT, plain_text_len, iv, plain_text, *output);
    //printf("After AES encrypt: ret=%d\n", ret);
    if (ret != 0) { goto encrypt_aes_cleanup; }
    //print_hex(*output, *output_len, "Encrypted buffer:");
    
    end_ret = 0;
    
encrypt_aes_cleanup:
    mbedtls_aes_free(&aes_ctx);
    
encrypt_end:
    return end_ret;
}


int decrypt_buffer(unsigned char **output, size_t *output_len, unsigned char *input, size_t input_len, unsigned char *symkey, size_t symkey_len, unsigned char *iv, size_t iv_len)
{
    mbedtls_aes_context aes_ctx;
    int ret, end_ret = 1;
    unsigned char i;
    
    
    if (output == NULL || output_len == NULL || input == NULL || input_len <= 0 || symkey == NULL || symkey_len <= 0 || iv == NULL || iv_len <= 0)
        goto decrypt_end;
    //printf("After tests\n");
    
    // Output alloc
    *output_len = input_len;
    if (*output == NULL)
    {
        *output = calloc(*output_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (*output == NULL));
        if (*output == NULL) goto decrypt_end;
    }
    //else printf("After output alloc (already allocated)\n");
    
    // Decrypt cipher text with sym key
    mbedtls_aes_init(&aes_ctx);
    
    ret = mbedtls_aes_setkey_dec(&aes_ctx, symkey, symkey_len * 8);
    if (ret != 0) { goto decrypt_aes_cleanup; }
    
    ret = mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_DECRYPT, input_len, iv, input, *output);
    //printf("After AES decrypt: ret=%d\n", ret);
    if (ret != 0) { goto decrypt_aes_cleanup; }
    //print_hex(*output, *output_len, "Decrypted buffer:");
    
    // Unpad output
    for (i = input_len - 1; i > (*output_len - AES_BLOCK_SIZE) && (*output)[i] != 0x80; i--);
    if (i == (*output_len - AES_BLOCK_SIZE))
        printf("Unpadding failed using padding type 0x80");
    *output_len = i;
    
    end_ret = 0;
    
decrypt_aes_cleanup:
    mbedtls_aes_free(&aes_ctx);
    
decrypt_end:
    return end_ret;
}

int sign_pss (unsigned char **sign, size_t *sign_len, unsigned char *input, size_t input_len, mbedtls_pk_context *privkey_ctx)
{
    mbedtls_entropy_context entropy_ctx;
    mbedtls_ctr_drbg_context ctr_drbg_ctx;
    int ret, end_ret = 1, hash_len = 32;
    unsigned char hash[hash_len];
    
    if (sign == NULL || sign_len <= 0 || input == NULL || input_len <= 0 || privkey_ctx == NULL)
        goto sign_end;
    //printf("After tests\n");
    
    memset(&entropy_ctx, 0, sizeof(entropy_ctx));
    memset(&ctr_drbg_ctx, 0, sizeof(ctr_drbg_ctx));
    
    // RNG initialization
    mbedtls_entropy_init(&entropy_ctx);
    ret = mbedtls_ctr_drbg_seed(&ctr_drbg_ctx, mbedtls_entropy_func, &entropy_ctx, NULL, 0);
    if (ret != 0) { goto sign_cleanup; }
    
    // Output alloc
    *sign_len = RSA2048_BLOCK_SIZE;
    if (*sign == NULL)
    {
        *sign = calloc(*sign_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (*sign == NULL));
        if (*sign == NULL) goto sign_cleanup;
    }
    //else printf("After output alloc (already allocated)\n");
    
    // Hash and sign
    mbedtls_rsa_set_padding(mbedtls_pk_rsa(*privkey_ctx), MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);
    
    ret = mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), input, input_len, hash);
    //print_hex(hash, hash_len, "mbedtls_md hash:");
    if (ret != 0) { goto sign_cleanup; }
    
    ret = mbedtls_pk_sign(privkey_ctx, MBEDTLS_MD_SHA256, hash, 0, *sign, sign_len, mbedtls_ctr_drbg_random, &ctr_drbg_ctx);
    if (ret != 0) { goto sign_cleanup; }
    //printf("Signature length: %lu\n", *sign_len);
    //print_hex(*sign, *sign_len, "Signature:");
    
    end_ret = 0;
    
sign_cleanup:
    mbedtls_ctr_drbg_free(&ctr_drbg_ctx);
    mbedtls_entropy_free(&entropy_ctx);
    
sign_end:
    return end_ret;
}

int verify_pss (unsigned char *sign, size_t sign_len, unsigned char *input, size_t input_len, mbedtls_pk_context *pubkey_ctx)
{
    int ret, end_ret = 1, hash_len = 32;
    unsigned char hash[hash_len];
    
    if (sign == NULL || sign_len <= 0 || input == NULL || input_len <= 0 || pubkey_ctx == NULL)
        goto verify_end;
    //printf("After tests\n");
    
    mbedtls_rsa_set_padding(mbedtls_pk_rsa(*pubkey_ctx), MBEDTLS_RSA_PKCS_V21, MBEDTLS_MD_SHA256);
    
    ret = mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), input, input_len, hash);
    //print_hex(hash, hash_len, "mbedtls_md hash:");
    if (ret != 0) { goto verify_end; }
    
    end_ret = mbedtls_pk_verify(pubkey_ctx, MBEDTLS_MD_SHA256, hash, 0, sign, sign_len);
    //printf("verify ret: %d\n", end_ret);
    
verify_end:
    return end_ret;
}
