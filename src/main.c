# include "multi_protect.h"

# include "mbedtls/havege.h"
# include "mbedtls/pk.h"

# include <stdlib.h>
# include <stdio.h>
# include <string.h>


int read_file (const char *fname, unsigned char **output, size_t *output_len)
{
    FILE *fp = NULL;
    int ret = -1, bytes_read;
    
    fp = fopen(fname, "rb");
    if (fp == NULL) { ret = 1; goto read_file_end; }
    
    fseek(fp, 0, SEEK_END);
    *output_len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *output = calloc(*output_len, sizeof(unsigned char));
    if (*output == NULL) { ret = 3; goto read_file_end; }
    
    bytes_read = fread(*output, 1, *output_len, fp);
    printf("read_file: %d bytes read from file '%s', output size %lu bytes\n", bytes_read, fname, *output_len);
    if (bytes_read != *output_len) { ret = 2; goto read_file_end; }
    
    ret = 0;
    
read_file_end:
    fclose(fp);
    if (ret != 0)
        fprintf(stderr, "read_file: error %d happened with file '%s'\n", ret, fname);
    return ret;
}

int write_file (const char *fname, unsigned char *input, size_t input_len)
{
    FILE *fp = NULL;
    int ret = -1, bytes_written;
    
    fp = fopen(fname, "wb");
    if (fp == NULL) { ret = 1; goto write_file_end; }
    
    bytes_written = fwrite(input, 1, input_len, fp);
    printf("write_file: %d bytes written to file '%s', input size %lu bytes\n", bytes_written, fname, input_len);
    if (bytes_written != input_len) { ret = 4; goto write_file_end; }
    
    ret = 0;
    
write_file_end:
    fclose(fp);
    if (ret != 0)
        fprintf(stderr, "write_file: error %d happened with file '%s'\n", ret, fname);
    return ret;
}


/* Usage:
 * - Protection:
 * multi_protect -e <input_file> <output_file> <my_sign_priv.pem> <my_ciph_pub.pem> [user1_ciph_pub.pem ... [userN_ciph_pub.pem]]
 * - Déprotection:
 * multi_protect -d <input_file> <output_file> <my_priv_ciph.pem> <my_pub_ciph.pem> <sender_sign_pub.pem>
 * 
 * Exemple:
 * ./bin/multi_protect -e data_to_protect protected keys/sign_rsa2048_priv.pem keys/ciph_rsa2048_pub.pem keys/1_ciph_rsa2048_pub.pem keys/2_ciph_rsa2048_pub.pem
 * ./bin/multi_protect -d protected unprotected keys/1_ciph_rsa2048_priv.pem keys/1_ciph_rsa2048_pub.pem keys/sign_rsa2048_pub.pem
 */

int main (int argc, char** argv)
{
    mbedtls_havege_state hs;
    mbedtls_pk_context privkey, pubkey, sender_pubkey, *recipients_keys = NULL;
    int ret = 1, mode = -1;
    size_t symkey_len = 32, iv_len = 16, input_len = 0, output_len = 0, akey_len = RSA2048_BLOCK_SIZE, enc_len = 0, sign_len = RSA2048_BLOCK_SIZE, rcpt_keys_len = 0;
    unsigned char pk_count = 0, i, iv[iv_len], own_pubkey_hash[HASH_SIZE];
    unsigned char *input = NULL, *output = NULL, *symkey = NULL, *akey = NULL, *enc_buf = NULL, *sign = NULL, **recipients_key_hashes = NULL, *p = NULL;
    char *input_filename = NULL, *output_filename = NULL;

    if (argc >= 7 && strcmp(argv[1], "-e") == 0)
        mode = 0;
    else if (argc == 7 && strcmp(argv[1], "-d") == 0)
        mode = 1;
    else
    {
        printf("Usage:\nProtect:\n %1$s -e <input_file> <output_file> <my_sign_priv.pem> <my_ciph_pub.pem> [user1_ciph_pub.pem ... [userN_ciph_pub.pem]]\nUnprotect:\n %1$s -d <input_file> <output_file> <my_ciph_priv.pem> <my_ciph_pub.pem> <sender_sign_pub.pem>\n", argv[0]);
        ret = -1;
        goto end;
    }

    /* Data initialization */
    
    memset(&privkey, 0, sizeof(privkey));
    memset(&pubkey, 0, sizeof(pubkey));
    memset(&sender_pubkey, 0, sizeof(sender_pubkey));
    
    input_filename = argv[2];
    output_filename = argv[3];
    ret = read_file(input_filename, &input, &input_len);
    if (ret != 0) goto key_load_cleanup;
    
    // Load own keys
    ret = load_pk_key(&privkey, 0, argv[4], NULL);
    if (ret != 0) goto key_load_cleanup;
    ret = load_pk_key(&pubkey, 1, argv[5], NULL);
    if (ret != 0) goto key_load_cleanup;
    
    if (mode == 1)
    {
        // Load sender key
        ret = load_pk_key(&sender_pubkey, 1, argv[6], NULL);
        if (ret != 0) goto key_load_cleanup;
    }
    else if (mode == 0)
    {
        /* Allocations of keys and hashes */
        recipients_keys = malloc((argc - 6) * sizeof(mbedtls_pk_context));
        recipients_key_hashes = malloc((argc - 6) * sizeof(unsigned char*));
        for (i = 0; i < (argc - 6); i++)
            recipients_key_hashes[i] = calloc(HASH_SIZE, sizeof(unsigned char));
        
        for (pk_count = 0; pk_count < argc - 6; pk_count++)
        {
            /* Load recipients keys */
            mbedtls_pk_init(&recipients_keys[pk_count]);
            ret = load_pk_key(&recipients_keys[pk_count], 1, argv[pk_count + 6], NULL);
            if (ret != 0) break;
            
            /* Public key hash */
            ret = mbedtls_md_file(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), argv[pk_count + 6], recipients_key_hashes[pk_count]);
            if (ret != 0) break;
            printf("Hash #%d: ", pk_count);
            print_hex(recipients_key_hashes[pk_count], HASH_SIZE, NULL);
        }
        
        // Cleanup if something goes wrong
        if (pk_count != argc - 6)
        {
            printf("Could not load some recipient key!");
            goto key_load_cleanup;
        }
    }

    //print_hex(input, input_len, "Input:");

    /* Do operation */
    if (mode == 0)
    {
        rcpt_keys_len = (1 + akey_len + HASH_SIZE) * pk_count + 1;
        enc_len = (input_len + AES_BLOCK_SIZE);
        
        /* Generate IV */
        mbedtls_havege_init(&hs);
        ret = mbedtls_havege_random(&hs, iv, iv_len);
        //memcpy(iv, IV, iv_len);
        print_hex(iv, iv_len, "Generated IV:");
        
        /* Symkey alloc */
        symkey = calloc(symkey_len, sizeof(unsigned char));
        //printf("After symkey alloc: ret=%d\n", (symkey == NULL));
        if (symkey == NULL) goto cleanup;
        
        /* Symmetric key generation */
        ret = mbedtls_havege_random(&hs, symkey, symkey_len);
        if (ret != 0) goto cleanup;
        print_hex(symkey, symkey_len, "Generated symmetric key:");

        /* Output alloc. Gross computation. Might need realloc. */
        output_len = rcpt_keys_len + iv_len + enc_len + sign_len; 
        output = calloc(output_len, sizeof(unsigned char));
        //printf("After output alloc: ret=%d\n", (output == NULL));
        if (output == NULL) goto cleanup;
        
        printf("\n");
        p = output;
    
        for (i = 0; i < pk_count; i++)
        {
            *p++ = 0x0;
            /* Public key registration */
            // akey, recipients_keys, pubkey
            ret = register_symkey (&akey, &akey_len, symkey, symkey_len, &recipients_keys[i]);
            if (ret != 0) goto cleanup;
            //print_hex(akey, akey_len, "Registered symkey:");
            printf("Registered symkey #%u: ", i);
            print_hex(recipients_key_hashes[i], HASH_SIZE, NULL);
            
            memcpy(p, recipients_key_hashes[i], HASH_SIZE);
            p += HASH_SIZE;
            memcpy(p, akey, akey_len);
            p += akey_len;
        }
        
        *p++ = 0x1;
        memcpy(p, iv, iv_len);
        p += iv_len;
        
        /* Data encryption */
        // warning: updates IV!
        ret = encrypt_buffer (&enc_buf, &enc_len, input, input_len, symkey, symkey_len, iv, iv_len); 
        if (ret != 0) goto cleanup;
        //print_hex(enc_buf, enc_len, "Encrypted buffer:");
        
        memcpy(p, enc_buf, enc_len);
        p += enc_len;
        output_len = p - output;
        
        /* Output signature */
        ret = sign_pss (&sign, &sign_len, output, output_len, &privkey); 
        if (ret != 0) goto cleanup;
        print_hex(sign, sign_len, "Signature:");
        
        memcpy(p, sign, sign_len);
        p += sign_len;
        output_len = p - output;
        //print_hex(output, output_len, "Output:");
        
        ret = write_file(output_filename, output, output_len);
    }
    else
    {
        /* Signature alloc */
        sign = calloc(sign_len, sizeof(unsigned char));
        //printf("After sign alloc: ret=%d\n", (sign == NULL));
        if (sign == NULL) goto cleanup;
    
        memcpy(sign, input + input_len - sign_len, sign_len);
        print_hex(sign, sign_len, "Signature:");
        
        /* Verify signature */
        ret = verify_pss (sign, sign_len, input, (input_len - sign_len), &sender_pubkey); 
        if (ret != 0) goto cleanup;
        printf("Signature verified!\n");
    
        /* Authentication key alloc */
        akey = calloc(akey_len, sizeof(unsigned char));
        //printf("After akey alloc: ret=%d\n", (akey == NULL));
        if (akey == NULL) goto cleanup;
    
        /* Hash own pubkey */
        ret = mbedtls_md_file(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), argv[5], own_pubkey_hash);
        if (ret != 0) goto cleanup;
        print_hex(own_pubkey_hash, HASH_SIZE, "Own pubkey hash:");
    
        /* Split input buffer */
        p = input;
        pk_count = 0;
        while (p < input + input_len && *p == 0x0)
        {
            p++;
            printf("Hash #%d: ", pk_count);
            pk_count++;
            
            print_hex(p, HASH_SIZE, NULL);
            if (memcmp(own_pubkey_hash, p, HASH_SIZE) == 0)
            {
                printf("Found akey!\n");
                memcpy(akey, p + HASH_SIZE, akey_len);
            }
            p += HASH_SIZE + akey_len;
        }
        if (p <= input || p >= input + input_len || *p != 0x1)
        {
            printf("Could not find your key in the recipients key list.\nEither the file is corrupted or you are not a legitimate recipient of this data.\n");
            goto cleanup;
        }
        rcpt_keys_len = (1 + akey_len + HASH_SIZE) * pk_count + 1;
        
        enc_len = (input_len - rcpt_keys_len - iv_len - sign_len);
        printf("input_len %lu, rcpt_keys_len %lu, akey_len %lu, iv_len %lu, enc_len %lu, sign_len %lu\n", input_len, rcpt_keys_len, akey_len, iv_len, enc_len, sign_len);
        
        /* Encrypted buffer alloc */
        enc_buf = calloc(enc_len, sizeof(unsigned char));
        //printf("After enc_buf alloc: ret=%d\n", (enc_buf == NULL));
        if (enc_buf == NULL) goto cleanup;
        
        p++;
        memcpy(iv, p, iv_len);
        p += iv_len;
        memcpy(enc_buf, p, enc_len);
        p += enc_len;
        
        //print_hex(akey, akey_len, "Authenticated key:");
        print_hex(iv, iv_len, "IV:");
        //print_hex(enc_buf, enc_len, "Encrypted buffer:");
        
        /* Public key authentication */
        // akey, sender_pubkey, privkey
        ret = authenticate_symkey (&symkey, &symkey_len, akey, akey_len, &privkey);
        if (ret != 0) goto cleanup;
        print_hex(symkey, symkey_len, "Symkey:");
        
        /* Data decryption */
        ret = decrypt_buffer (&output, &output_len, enc_buf, enc_len, symkey, symkey_len, iv, iv_len);
        if (ret != 0) goto cleanup;
        print_hex(output, output_len, "Unprotected buffer:");
        
        ret = write_file(output_filename, output, output_len);
    }

    ret = 1;

cleanup:
    mbedtls_havege_free(&hs);
    if (symkey != NULL) zeroize(symkey, symkey_len);

key_load_cleanup:
    if (input != NULL) free(input);
    if (output != NULL) free(output);
    if (symkey != NULL) free(symkey);
    if (akey != NULL) free(akey);
    if (enc_buf != NULL) free(enc_buf);
    if (sign != NULL) free(sign);
    
    if (mode == 0)
    {
        for (i = 0; i < pk_count; i++)
        {
            mbedtls_pk_free(&recipients_keys[i]);
            if (recipients_key_hashes[i] != NULL) free(recipients_key_hashes[i]);
        }
        if (recipients_keys != NULL) free(recipients_keys);
        if (recipients_key_hashes != NULL) free(recipients_key_hashes);
    }
    mbedtls_pk_free(&privkey);
    mbedtls_pk_free(&pubkey);
    mbedtls_pk_free(&sender_pubkey);

end:
    return ret;
}
